import os
import json
import gzip
import pandas as pd
from urllib.request import urlopen

data = []
with gzip.open('meta_Tools_and_Home_Improvement.json.gz') as f:
    for l in f:
        data.append(json.loads(l.strip()))
    
# total length of list, this number equals total number of products
print(len(data))

# first row of the list
print(data[0])
df = pd.DataFrame.from_dict(data)
df3 = df.fillna('')
df4 = df3[df3.title.str.contains('getTime')] # unformatted rows
df5 = df3[~df3.title.str.contains('getTime')] # filter those unformatted rows
# how those unformatted rows look like
df4.iloc[0]
df.head(1000).to_csv('AmazonAppliances.csv', sep=',', encoding='utf-8')
